
<?php include 'header.php' ;?>
  <div class="preload" style="position:fixed;height:100%;width:100%;z-index:9999;background-color:#000;">
      
  </div>
   <div class="flt-bg"> 
       <div class="bg-left"></div>
       <div class="bg-right"></div>
   </div>
   
   <div class="container fltng middle">
         
        <div class="embed-responsive embed-responsive-16by9">
          <iframe width="100%" src="https://www.youtube.com/embed/hmPgE6b8FxU" frameborder="0" allowfullscreen></iframe>
        </div>
        
        <div class="row-fluid text-center btn-hm">
                <button class="btns btn-grn mrgR20" data-toggle="modal" data-target="#mchncs">Mechanics</button>
                <button class="btns btn-grn" data-toggle="modal" data-target="#upld">Upload</button>
        </div>
      
   </div>
   
   
   <!-- pop up for upload -->
   
<div class="modal fade" id="upld" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content frm-modal">
      <div class="modal-header">
        <button type="button" class="close cs-cls" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
          <form class="frm">
                <div class="row">               
                   <div class="col-md-4">
                       <label>First Name</label>
                   </div>
                   <div class="col-md-8">
                       <input type="text">
                   </div>               
                </div>
                <div class="row">               
                   <div class="col-md-4">
                       <label>Last Name</label>
                   </div>
                   <div class="col-md-8">
                       <input type="text">
                   </div>               
                </div>
                <div class="row">               
                   <div class="col-md-4">
                       <label>Address</label>
                   </div>
                   <div class="col-md-8">
                       <textarea rows="5"></textarea>
                   </div>               
                </div>
                <div class="row">               
                   <div class="col-md-4">
                       <label>Birtday</label>
                   </div>
                   <div class="col-md-8 bdy">
                      <div class="cs-slct">
                       <select>
                           <option>January</option>
                           <option>January</option>
                           <option>January</option>
                           <option>January</option>
                           <option>January</option>
                           <option>January</option>
                       </select>
                       </div>
                       <div class="cs-slct">
                       <select>
                           <option>1</option>
                           <option>1</option>
                           <option>1</option>
                           <option>1</option>
                           <option>1</option>
                       </select>
                       </div>
                       <div class="cs-slct">
                       <select>
                           <option>2015</option>
                           <option>2015</option>
                           <option>2015</option>
                           <option>2015</option>
                           <option>2015</option>
                           <option>2015</option>                           
                       </select>
                       </div>
                   </div>
                </div>   
               <div class="row">               
                   <div class="col-md-4">
                       <label>Contact No.</label>
                   </div>
                   <div class="col-md-8">
                       <input type="text">
                   </div>               
                </div>
                <div class="row">               
                   <div class="col-md-4">
                       <label>Email Address</label>
                   </div>
                   <div class="col-md-8">
                       <input type="text">
                   </div>               
                </div>
                <div class="row">               
                   <div class="col-md-4">
                       <label>Coupon Code</label>
                   </div>
                   <div class="col-md-8">
                       <input type="text">
                   </div>               
                </div> 
                <div class="row">
                    <div class="col-md-8 col-md-offset-4">           
                       <button class="btns btnsm btn-grn">Browse File</button>   
                    </div>         
                </div> 

                <div class="row text-center">               
                   <button class="btns btn-grn">Upload</button>            
                </div>               
           </form>
      </div>
    </div>
  </div>
</div>
   
   <!-- pop up for mechanics -->
   
<div class="modal fade" id="mchncs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content frm-modal">
      <div class="modal-header">
        <button type="button" class="close cs-cls" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body frm">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit voluptatum eum aut aspernatur neque magni facere exercitationem error ut rem illo, aperiam ullam in, reiciendis, optio. Nesciunt reiciendis distinctio aspernatur!</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit voluptatum eum aut aspernatur neque magni facere exercitationem error ut rem illo, aperiam ullam in, reiciendis, optio. Nesciunt reiciendis distinctio aspernatur!</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit voluptatum eum aut aspernatur neque magni facere exercitationem error ut rem illo, aperiam ullam in, reiciendis, optio. Nesciunt reiciendis distinctio aspernatur!</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit voluptatum eum aut aspernatur neque magni facere exercitationem error ut rem illo, aperiam ullam in, reiciendis, optio. Nesciunt reiciendis distinctio aspernatur!</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit voluptatum eum aut aspernatur neque magni facere exercitationem error ut rem illo, aperiam ullam in, reiciendis, optio. Nesciunt reiciendis distinctio aspernatur!</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit voluptatum eum aut aspernatur neque magni facere exercitationem error ut rem illo, aperiam ullam in, reiciendis, optio. Nesciunt reiciendis distinctio aspernatur!</p>
      </div>
    </div>
  </div>
</div>
    
<?php include 'footer.php' ;?>
